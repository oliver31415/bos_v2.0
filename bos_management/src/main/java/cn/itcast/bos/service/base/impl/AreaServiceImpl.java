package cn.itcast.bos.service.base.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.base.AreaRepository;
import cn.itcast.bos.service.base.AreaService;
import cn.itcast.bos.domain.base.Area;

@Service
@Transactional
public class AreaServiceImpl implements AreaService {
	
	@Autowired
	private AreaRepository areaRepository;
	
	@Override
	public void saveBatch(List<Area> areas) {
		areaRepository.save(areas);
	}

	@Override
	public Page<Area> findPageData(Specification<Area> specification, Pageable pageable) {
		return areaRepository.findAll(specification, pageable);
	}

	@Override
	public void save(Area area) {
		areaRepository.save(area);
	}

	@Override
	public void delBatch(String[] idArray) {
		for (String idStr : idArray) {
			areaRepository.delete(idStr);
		}
	}
	
}
