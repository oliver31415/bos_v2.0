package cn.itcast.crm.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import cn.itcast.crm.domain.Customer;

public interface CustomerService {
	
	@Path("noassociationcustomers")
	@GET
	@Produces({"application/xml", "application/json"})
	public List<Customer> findNoAssociationCustomers();
	
	@Path("associationfixedareacustomers/{fixedareaid}")
	@GET
	@Produces({"application/xml", "application/json"})
	public List<Customer> findHasAssociationFixedAreaCustomers(@PathParam("fixedareaid") String fixedAreaId);
	
	@Path("/associationcustomerstofixedarea")
	@PUT
	public void associationCustomersToFixedArea(
			@QueryParam("customerIdStr") String customerIdStr,
			@QueryParam("fixedAreaId") String fixedAreaId
			);
	
	@Path("/customer")
	@POST
	@Consumes({"application/xml", "application/json"})
	public void regist(Customer customer);
	
	@Path("/customer/telephone/{telephone}")
	@GET
	@Consumes({"application/xml", "application/json"})
	public Customer findByTelephone(@PathParam("telephone") String telephone);
	
	@Path("/customer/updatetype/{telephone}")
	@GET
	public void updateType(@PathParam("telephone") String telephone);
	
	//
	@Path("customer/login")
	@GET
	@Consumes({ "application/xml", "application/json" })
	public Customer login(@QueryParam("telephone") String telephone, @QueryParam("password") String password);
	
	//
	@Path("/customer/findFixedAreaIdByAddress")
	@GET
	@Consumes({ "application/xml", "application/json" })
	public String findFixedAreaIdByAddress(@QueryParam("address") String address);
}
