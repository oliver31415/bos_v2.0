package cn.itcast.bos.redis.test;

import org.junit.Test;

import redis.clients.jedis.Jedis;

public class JedisTest {
	@Test
	public void testRedis() {
		Jedis jedis = new Jedis("localhost");
		jedis.setex("test1", 30, "test1-value");
		System.out.println(jedis.get("test1"));
		System.out.println(jedis.get("company"));
	}
}
